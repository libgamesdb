#!/bin/env python
"""
Limf
----
Limf is a tool for uploading files to pomf.se clones.
"""
from setuptools import setup


setup(
    name='libgamesdb',
    version='0.0.2',
    url='http://repo.or.cz/libgamesdb.git',
    license='MIT',
    author='Mikolaj \'lich\' Halber',
    author_email='lich@openmailbox.com',
    description='A library for http://thegamesdb.net/',
    packages=['libgamesdb']
)




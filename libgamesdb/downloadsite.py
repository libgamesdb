import urllib.request as request

def openurl(url):
    """Opens url"""
    req = request.Request(
        url,
        data=None,
        headers={
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) ' \
            ' AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.47 Safari/537.36'
            }
        )
    return request.urlopen(req)

def main():
    """Main function for testing"""
    openurl('http://example.com')

if __name__ == '__main__':
    main()

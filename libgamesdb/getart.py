"""
Gets arts of a game from TheGamesDB
"""
import xml.etree.ElementTree as ET
from .downloadsite import openurl

def getart(game_id: int):
    """
    Returns dictionary with links to files and base url to add to the urls
    Example: getart(x)[0]+getart(x)[type_of_art]['img'][1]
    """
    url = "http://thegamesdb.net/api/GetArt.php?id={}".format(str(game_id))
    art_data = openurl(url).read().decode('utf-8')
    root = ET.fromstring(art_data)
    base_url = root[0].text
    file_dict = {}
    for child in root[1]:
    #This cluster hell is necessary as theGamesDB xmls are very chaotic
        if child.attrib == {}:
            for element in child:
                if 'thumb' in element.text:
                    thumb = element.text
                else:
                    img = element.text
        elif 'thumb' in child.attrib:
            img = child.text
            thumb = child.attrib['thumb']
        else:
            img = child.text
            thumb = ''
        if child.tag not in file_dict.keys():
            file_dict[child.tag] = {'img':[img], 'thumb':[thumb]}
        else:
            file_dict[child.tag]['img'].append(img)
            file_dict[child.tag]['thumb'].append(thumb)
    return [base_url, file_dict]

def main():
    """
    Main function for testing
    """
    print(getart(22))

if __name__ == '__main__':
    main()
